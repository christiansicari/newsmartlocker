// rfid
#include <MFRC522.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>

// screen 
#include <LiquidCrystal_I2C.h>

//http
#include <Wire.h>
#include <UnoWiFiDevEd.h>

//rfid
#define SS_PIN 10
#define RST_PIN 9

// interrupts
#include <avr/interrupt.h>
#include <avr/io.h>

// sys
#define UNLOCKEDTIME 3000
#define DEBOUNCEDELAY 300
#define TIMEOUT  62500

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
LiquidCrystal_I2C lcd(0x27,20,4);
int bl = 1;
int btn = 0;
unsigned long lastDebounceTime = 0;
unsigned int count = 0;
const char* server = "90.147.188.79";
const char* connector = "rest";
const char* method = "GET";
String path = "/uid/";

void setup() {
  
  Serial.begin(9600);
  //rfid
  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522 p
  
  //screen 
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  
  // http
  Ciao.begin();

  // interrupts
  initInterrupt0();
  timer1_init();
  sei();  

  // A1 => Relay
  DDRC |= 1<<1;
  PORTC |= 1<<1;
  msg_disp("Ready","","","");
}

void loop() {
  if(btn)
  {
    TCNT1=0;
    mngLight(!bl);
    btn = 0;
  }
  if(  rfid.PICC_IsNewCardPresent())
  {
     // Restart timer
     count = 0;
     
     mngLight(1);
     String uid = readRFID();
     // Serial.println(uid);

     uid = uid.substring(0,8);
     msg_disp("RFID Read" , uid,"","");

    String reqStatus = path + uid;
    String reqBody = doRequest(connector, server, reqStatus, method);
    reqStatus = getValue(reqBody,',',0);
    reqBody = getValue(reqBody,',',1);

    if(reqStatus == "200"){
      unlock(reqBody);
      
    }
    else if(reqStatus =="403"){
      msg_disp("NOT ALLOWED", reqBody, "", "");
      
    }
    else if(reqStatus =="404"){
      msg_disp("RFID NOT FOUND", "","","");
      
    }
    else{
      msg_disp("GENERIC ERROR", "","","");
      
    }
    delay(3000);
    msg_disp("Ready", "","","");
     
  }

}

ISR(TIMER1_COMPA_vect)
{
    // Serial.println("timer interrupt");
    if(count==3)
    {
      Serial.println("TIMER");
      // Serial.println("timer into if");
     //(-1);
     if(bl)
        btn=1;
      count=0;
      // Serial.println("Screen off");
    }
    count++;
}

void timer1_init()
{
    TCCR1B = 0;
    TCCR1A = 0;
    // set up timer CTC mode
    TCCR1B |= (1 << WGM12);
    // prescaler
    TCCR1B |= (1 << CS12)|(1 << CS10);
    TCCR1B  &=~(1 << CS11);

    // initialize counter
    TCNT1 = 0;

    // initialize compare value
    OCR1A = TIMEOUT;

    // enable compare interrupt
    TIMSK1 |= (1 << OCIE1A);
}


ISR(INT0_vect)//pin2
{
  if ((millis() - lastDebounceTime) > DEBOUNCEDELAY)
  {
    //mngInt(-bl);
    TCNT1=0;
    count = 0;
    btn = 1;
    // Serial.println("---->interrupt: " + String(btn));
  }
  lastDebounceTime = millis();
}

void initInterrupt0(void) {
  EIMSK |= (1 << INT0);//abilitazione pin2 interrupt
  EICRA |=  (1 << ISC00); // sensibilita su ogni fronte
}

void mngLight(int light){
  if(light == 1){
    bl = 1;
    lcd.backlight();
  }
  else{
    bl = 0;
    lcd.noBacklight();
  }
}

void unlock(String msg){
  msg_disp("ALLOWED", msg, "", "");
  PORTC &= ~(1<<1);
  delay(UNLOCKEDTIME);
  PORTC |= 1<<1;
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}


String doRequest(const char* conn, const char* server, String command, const char* method){
  // Serial.println("Im doing the request");
  CiaoData data = Ciao.write(conn, server, command, method);
  if (!data.isEmpty()){
    Ciao.println( "State: " + String (data.get(1)) );
    Ciao.println( "Response: " + String (data.get(2)) );
    //// Serial.println( "data.get(1): " + String (data.get(1)) );
    //// Serial.println( "data.get(2): " + String (data.get(2)) );
    return data.get(2);
  }
  else{
    Ciao.println ("Write Error");
  // Serial.println ("Write Error");
    return "500,error";
  }
}



void msg_disp(String m1,String m2,String m3,String m4){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(m1);
  lcd.setCursor(0,1);
  lcd.print(m2);
  lcd.setCursor(0,2);
  lcd.print(m3);
  lcd.setCursor(0,3);
  lcd.print(m4);  
}

String readRFID()
{
  String uidString;
  rfid.PICC_ReadCardSerial();
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  
  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
    piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
    piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    // Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  } 
  uidString = String(rfid.uid.uidByte[0])+String(rfid.uid.uidByte[1])+String(rfid.uid.uidByte[2])+String(rfid.uid.uidByte[3]);
  return uidString;
}
